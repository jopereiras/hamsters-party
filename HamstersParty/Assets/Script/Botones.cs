﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Obsolete("Obsoleto: Usar CustomCharacterInput.")]
public class Botones : MonoBehaviour {

    private Rigidbody2D Rb2d;
    public Transform BalaSpawner;
    public GameObject BalaPrefab;
    public float FuerzaSalto;
    public float FuerzaMax;
    public bool salto;
    Vector2 Fue;
    Vector2 FueMax = new Vector2(0, 5);

    void Start () {
        Rb2d = GetComponent<Rigidbody2D>();
    }
	
    void FixedUpdate()
    {
        Fue = Rb2d.velocity;
    }

    public void Salto()
    {

        if (Input.GetKeyDown(KeyCode.Space) && salto == false)
        {
            Rb2d.AddForce(Vector2.up * FuerzaSalto, ForceMode2D.Impulse);

            float LimiteFuerza = Mathf.Clamp(Rb2d.velocity.y, -FuerzaMax,FuerzaMax );
            Rb2d.velocity = new Vector2(Rb2d.velocity.x, LimiteFuerza);




        }
        if (Input.GetKeyUp(KeyCode.Space) && Fue.y > FueMax.y)
        {
            
            Rb2d.AddForce(Vector2.down * (Fue.y / 2) , ForceMode2D.Impulse);

        }

    }

    public void PlayerShooting()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            Instantiate(BalaPrefab, BalaSpawner.position, BalaSpawner.rotation);
        }

    }

}
