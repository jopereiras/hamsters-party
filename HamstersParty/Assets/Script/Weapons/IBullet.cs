﻿using System;
using UnityEngine;

public interface IBullet 
{
    event EventHandler<OnBulletCollisionEventArgs> OnCollision;

    void OnBulletCollision(GameObject other);
    void Move();
}

public class OnBulletCollisionEventArgs : EventArgs
{
    public readonly GameObject other;
    public readonly int damage;

    public OnBulletCollisionEventArgs(GameObject other, int damage)
    {
        this.other = other;
        this.damage = damage;
    }
}
