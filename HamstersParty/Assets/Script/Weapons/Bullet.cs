﻿using System;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Bullet : MonoBehaviour, IBullet
{
    [SerializeField] float speed = 10f;
    protected Collider2D coll;
    [SerializeField] int damage = 1;
    [SerializeField] protected float timeToDestroy = 5f;

    public event EventHandler<OnBulletCollisionEventArgs> OnCollision;

    private void Awake()
    {
        coll = GetComponent<Collider2D>();
    }
    private void Start()
    {
        Destroy(gameObject, timeToDestroy);
    }

    private void Update()
    {
        Move();
    }

    public virtual void Move()
    {
        transform.Translate(Vector2.right * speed * Time.deltaTime, Space.Self);
    }

    /// <summary>
    /// Hace que la bala colisione con otro objeto.
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        OnBulletCollision(collision.gameObject);
    }

    /// <summary>
    /// Lo que hace la bala al colisionar.
    /// </summary>
    /// <param name="other"></param>
    public virtual void OnBulletCollision(GameObject other)
    {
        ICharacter character = other.GetComponent<ICharacter>();
        if (character != null && character.GetTransform().tag != tag)
        {
            coll.enabled = false;
            character.SetDamage(damage);
            Destroy(gameObject, 0.3f);
        }
        else if (other == null)
        {
            coll.enabled = false;
            Destroy(gameObject, 0.3f);
        }

        OnCollision?.Invoke(this, new OnBulletCollisionEventArgs(other, damage));
    }

}
