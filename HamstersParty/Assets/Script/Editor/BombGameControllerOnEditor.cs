﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(BombGameController))]
public class BombGameControllerOnEditor : Editor
{
    int numberOfPlayers;
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        BombGameController controller = (BombGameController)target;
        numberOfPlayers = EditorGUILayout.IntField("Players to Instantiate", numberOfPlayers);

        if(!controller.GameHasStart && GUILayout.Button("Start Game"))
        {
            controller.StartGame(numberOfPlayers);
        }
    }
}
