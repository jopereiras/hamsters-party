﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(DeathMatchGameController))]
public class DeathMatchGameControllerOnEditor : Editor
{
    int numberOfPlayers;
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        DeathMatchGameController controller = (DeathMatchGameController)target;
        numberOfPlayers = EditorGUILayout.IntField("Players to Instantiate", numberOfPlayers);

        if (!controller.GameHasStart && GUILayout.Button("Start Game"))
        {
            controller.StartGame(numberOfPlayers);
        }
    }
}
