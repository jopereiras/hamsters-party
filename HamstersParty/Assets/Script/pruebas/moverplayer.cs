﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moverplayer : MonoBehaviour {

    public bool congelar = false;
    [SerializeField] Transform agarrar;
    Rigidbody2D rig;

    private void Awake()
    {
        rig = GetComponent<Rigidbody2D>();
    }

    private void OnCollisionStay2D(Collision2D col)
    {
        if (col.gameObject.tag == "Player" && congelar == true )
        {
            transform.parent = col.transform;
            rig.simulated = false;
        }
        if (col.gameObject.tag == "Player" && congelar == false )
        {
            transform.parent = null;
            rig.simulated = true;
        }


    }

    private void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.layer == LayerMask.NameToLayer("Bullet"))
        {
            congelar = true;
        }      
            
    }



}

