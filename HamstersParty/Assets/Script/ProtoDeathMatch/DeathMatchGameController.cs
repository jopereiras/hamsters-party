﻿using HamsterParty.Lobby;
using System;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class DeathMatchGameController : MonoBehaviour, IGameController
{
    public const string GAME_MODE = "game.mode.death_match";

    public static DeathMatchGameController Instance { get; private set; }

    public event EventHandler<EndGameEventArgs> OnEndGame;
    public event EventHandler<StartGameEventArgs> OnStartGame;
    public event EventHandler<PauseGameEventArgs> OnPauseGame;

    [SerializeField] PlayersLifeUI UIplayersLife;

    public ushort maxPlayer = 4;
    public ushort MaxPlayer { get { return maxPlayer; } }

    private bool isGameOver;
    public bool GameHasStart { get; private set; }

    [SerializeField] Character characterPrefab;
    [SerializeField]
    private Color[] playerColours = new Color[4]
    {
        Color.red,
        Color.blue,
        Color.green,
        Color.magenta,
    };
    [SerializeField] Transform[] spawnPoints = new Transform[4];

    private ICharacter[] initialCharacters;

    private List<ICharacter> activeCharacters;
    public ICharacter[] ActivePlayers
    {
        get { return activeCharacters.ToArray(); }
    }

    void Awake()
    {
        Instance = this;

        CharacterLobbyManager.OnLoaded += CharacterLobbyManager_OnLoaded;
        CharacterLobbyManager.OnSaved += CharacterLobbyManager_OnSaved;
        CharacterLobbyManager.LoadPlayersData();
    }

    /// <summary>
    /// De momento el controlador no hace nada cuando se guarda exitosamente el archivo json.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void CharacterLobbyManager_OnSaved(object sender, bool e)
    {
        //throw new NotImplementedException();
    }

    /// <summary>
    /// Crear los jugadores a partir de los datos del json
    /// DEBUG: entonces llama la funcion por defecto con 4 jugadores.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="sucessful"></param>
    private void CharacterLobbyManager_OnLoaded(object sender, bool sucessful)
    {
        PlayerInfo[] lobbyPlayers = CharacterLobbyManager.PlayersInfo;
        if (!sucessful || lobbyPlayers.Length < 2)
        {
            #if (INTERNAL_DEBUG)
            {
                StartGame(MaxPlayer);
                return;
            }
            #else
            {
                Debug.LogError("NO hay datos de jugadores que cargar");
                EndGame();
                return;
            }
            #endif
        }

        
        ICharacter[] tempCharacters = new ICharacter[lobbyPlayers.Length];
        for (int i = 0; i < tempCharacters.Length; i++)
        {
            tempCharacters[i] = Instantiate(characterPrefab, Vector3.zero, Quaternion.identity).GetComponent<ICharacter>();
            tempCharacters[i].GetTransform().gameObject.SetActive(false);

            // Nombre, tag y color provisionales. La idea es agregarlos respecto al orden en que se unieron a la partida
            tempCharacters[i].ChangeColour(lobbyPlayers[i].colour);
            tempCharacters[i].GetTransform().gameObject.name = "PlayerBomb0" + (i + 1);
            tempCharacters[i].GetTransform().gameObject.tag = "P" + (i + 1);
            //

            // Iniciar barra de vidas
            if (UIplayersLife != null)
            {
                UIplayersLife.Initialize(tempCharacters.Length - 1, Character.MAX_LIFE);
                UIplayersLife.SetColor(lobbyPlayers[i].colour, i);
                
            }

            tempCharacters[i].ID = lobbyPlayers[i].id;
            tempCharacters[i].GetTransform().GetComponent<CustomCharacterInput>().SetInputType(lobbyPlayers[i].Input);
        }
        
        // Iniciar barra de vidas
        if (UIplayersLife != null)
        {
            UIplayersLife.Initialize(tempCharacters.Length-1, Character.MAX_LIFE);
            for (int i = 0; i < lobbyPlayers.Length; i++)
            {
                UIplayersLife.SetColor(lobbyPlayers[i].colour, i);
            }
        }

        // Inicia el juego con los jugadores creados.
        StartGame(tempCharacters);
    }
   
    /// <summary>
    /// Revisa la vida de los jugadores cuando uno recive daño.
    /// Cambia la vida de los jugadores en la UI.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void Character_OnDamage(object sender, DamageEventHandler e)
    {
        ICharacter character = (ICharacter)sender;

        if (character == null || UIplayersLife == null) { return; }

        for (int i = 0; i < activeCharacters.Count; i++)
        {
            if (activeCharacters[i] == character)
            {
                UIplayersLife?.ChangeByPlayersIndex(i, e.LifeLeft);
                break;
            }
        }
        CheckForAlivePlayers();
    }

    public void StartGame(ICharacter[] characters)
    {
        initialCharacters = characters;
        List<Transform> activeSpawnPoint = new List<Transform>(spawnPoints);
        activeCharacters = new List<ICharacter>(initialCharacters);
        for (int i = 0; i < ActivePlayers.Length; i++)
        {
            int index = new Random().Next(activeSpawnPoint.Count);
            SpawnPlayer(i, activeSpawnPoint[index]);
            activeSpawnPoint.RemoveAt(index);
            ActivePlayers[i].OnDamage += Character_OnDamage;
        }

        isGameOver = false;
        GameHasStart = true;

        OnStartGame?.Invoke(this, new StartGameEventArgs(GAME_MODE));
    }

    public void StartGame(int numberOfPlayers)
    {
        numberOfPlayers = Mathf.Clamp(numberOfPlayers, 1, MaxPlayer);
        ICharacter[] tempCharacters = new ICharacter[numberOfPlayers];
        for (int i = 0; i < tempCharacters.Length; i++)
        {
            tempCharacters[i] = Instantiate(characterPrefab, Vector3.zero, Quaternion.identity).GetComponent<ICharacter>();
            tempCharacters[i].GetTransform().gameObject.SetActive(false);
            tempCharacters[i].ChangeColour(playerColours[i]);
            tempCharacters[i].GetTransform().gameObject.name = "Player0" + (i + 1);
            tempCharacters[i].GetTransform().gameObject.tag = "P" + (i + 1);
            tempCharacters[i].GetTransform().GetComponent<CustomCharacterInput>().SetInputType((InputType)i);
        }

        StartGame(tempCharacters);
    }

    public void EndGame()
    {
        print("Game has end");
        GameHasStart = false;
        isGameOver = true;
        /// TO-DO Order characters by the moment they died
        
        OnEndGame?.Invoke(this, new EndGameEventArgs(null));
    }

    public void PauseGame(bool value)
    {
        OnPauseGame?.Invoke(this, new PauseGameEventArgs(value));
    }

    public void RestartGame()
    {
        print("Game has end");
        GameHasStart = false;
        isGameOver = true;
        /// TO-DO Order characters by the moment they died
        OnEndGame?.Invoke(this, new EndGameEventArgs(null));
    }

    /// <summary>
    /// Spawn an Active Character by its index on Active player List
    /// </summary>
    /// <param name="index"></param>
    public void SpawnPlayer(int index)
    {
        SpawnPlayer(index, spawnPoints[new Random().Next(spawnPoints.Length)]);
    }

    /// <summary>
    /// Spawn an Active Character at specific point
    /// </summary>
    /// <param name="index"></param>
    /// <param name="point"></param>
    public void SpawnPlayer(int index, Transform point)
    {
        activeCharacters[index].GetTransform().position = point.position;
        activeCharacters[index].GetTransform().gameObject.SetActive(true);
    }

    void Update()
    {
        if(!GameHasStart || isGameOver) { return; }

        CheckForAlivePlayers();
    }

    private void CheckForAlivePlayers()
    {
        if(!isGameOver && activeCharacters.Count == 1)
        {
            EndGame();
            return;
        }

        for (int i = 0; i < activeCharacters.Count; i++)
        {
            if (activeCharacters[i].Life <= 0.0f)
            {
                GameObject tempPlayer = activeCharacters[i].GetTransform().gameObject;
                activeCharacters.Remove(activeCharacters[i]);
                Destroy(tempPlayer, 0.5f);
            }
        }
    }

}
