﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class CharacterFlag : MonoBehaviour, ICharacter
{
    public int Life
    {
        get; set;
    }

    private int id = -1;
    public int ID
    {
        get { return id; }
        set
        {
            if(id > 0 || value < 0) { return; }
            id = value;
        }
    }

    public event EventHandler<DamageEventHandler> OnDamage;

    public void ChangeColour(Color colour)
    {
        throw new NotImplementedException();
    }

    public void FastGround()
    {
        throw new NotImplementedException();
    }

    public Transform GetTransform()
    {
        throw new NotImplementedException();
    }

    public void Jump()
    {
        throw new NotImplementedException();
    }

    public void Move(float dir)
    {
        throw new NotImplementedException();
    }

    public void SetDamage(int damage)
    {
        throw new NotImplementedException();
    }

    public void Shoot()
    {
        throw new NotImplementedException();
    }
}
