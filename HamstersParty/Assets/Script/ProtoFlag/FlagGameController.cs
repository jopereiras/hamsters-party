﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class FlagGameController : MonoBehaviour, IGameController
{
    public ushort MaxPlayer { get; private set; }

    public ICharacter[] ActivePlayers { get; private set; }

    public event EventHandler<EndGameEventArgs> OnEndGame;
    public event EventHandler<StartGameEventArgs> OnStartGame;
    public event EventHandler<PauseGameEventArgs> OnPauseGame;

    public void EndGame()
    {
        throw new NotImplementedException();
    }

    public void PauseGame(bool value)
    {
        throw new NotImplementedException();
    }

    public void RestartGame()
    {
        throw new NotImplementedException();
    }

    public void SpawnPlayer(int index)
    {
        throw new NotImplementedException();
    }

    public void SpawnPlayer(int index, Transform point)
    {
        throw new NotImplementedException();
    }

    public void StartGame(ICharacter[] characters)
    {
        throw new NotImplementedException();
    }

    public void StartGame(int numberOfPlayers)
    {
        throw new NotImplementedException();
    }
}