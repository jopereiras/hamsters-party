﻿public interface ICharacterBomb : ICharacter
{
    //Bomb Bomb { get; }
    bool HasBomb();
    void AddBomb(ref Bomb bomb);
    void RemoveBomb();
    void AddDamageForce(int directon);
}