﻿using System.Collections;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    SpriteRenderer rend;    
    ICharacterBomb targetCharacter;
    public ICharacterBomb TargetCharacter
    {
        get { return targetCharacter; }
        set
        {
            transform.parent = value?.GetTransform();
            targetCharacter = value;
            if (value != null)
            {
                MoveToTarget(targetCharacter.GetTransform());
                StartCoroutine("WaitToReset", 3f);
                StartCoroutine("WaitToDoDamage");
            }
            else
            {
                StopCoroutine("WaitToDoDamage");
            }
        }
    }

    [SerializeField] int damagePerSecond = 2;
    public int DamagePerSecond
    {
        get { return damagePerSecond; }
        set { if(value >= 0) { damagePerSecond = value; } }
    }

    private void Start()
    {
        rend = GetComponent<SpriteRenderer>();
        BombGameController.Instance.OnEndGame += Instance_OnEndGame;
    }

    private void Instance_OnEndGame(object sender, EndGameEventArgs e)
    {
        StopAllCoroutines();
    }

    /// <summary>
    /// La bomba vuelve a su estado original
    /// </summary>
    public void Reset()
    {
        gameObject.layer = LayerMask.NameToLayer("Bomb");
        rend.material.color = Color.white;
    }

    void MoveToTarget(Transform target)
    {
        /// Test
        transform.localPosition = Vector3.zero;
    }

    IEnumerator WaitToDoDamage()
    {
        if(damagePerSecond == 0)
        {
            yield break;
        }
        yield return new WaitForSeconds(1);
        targetCharacter.Life -= damagePerSecond;

        StartCoroutine("WaitToDoDamage");
    }

    /// <summary>
    /// Gatilla la animación de la bomba.
    /// </summary>
    public void Explosion(float timeToReset = 1)
    {
        StopAllCoroutines();
        print("BOOOOOMB");
        TargetCharacter = null;
        rend.material.color = Color.red;
        StartCoroutine(WaitToReset(timeToReset));
    }

    private IEnumerator WaitToReset(float t)
    {
        gameObject.layer = LayerMask.NameToLayer("BombDontCollide");
        yield return new WaitForSeconds(t);
        Reset();
    }
}
