﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;
using HamsterParty.Lobby;

public class BombGameController : MonoBehaviour, IGameController
{
    public const string GAME_MODE = "game.mode.bomb";

    /// <summary>
    /// Singleton del minijuego
    /// </summary>
    public static BombGameController Instance { get; private set; }

    public event EventHandler<EndGameEventArgs> OnEndGame;
    public event EventHandler<StartGameEventArgs> OnStartGame;
    public event EventHandler<PauseGameEventArgs> OnPauseGame;

    [SerializeField] CharacterBomb characterPrefab;
    /// <summary>
    /// Lista de colores que representan a los personajes.
    /// Se puede modificar en el inspector de Unity
    /// </summary>
    [SerializeField] private Color[] playerColours = new Color[4]
    {
        Color.red,
        Color.blue,
        Color.green,
        Color.magenta,
    };

    /// <summary>
    /// Jugadores que entran al minijuego
    /// </summary>
    private ICharacter[] initialCharacters;

    private List<ICharacterBomb> activePlayers;
    /// <summary>
    /// Jugadores que estan vivos
    /// </summary>
    public ICharacter[] ActivePlayers
    {
        get { return activePlayers.ToArray(); }
    }

    [SerializeField] Bomb bombPrefab;
    [HideInInspector] public Bomb theBomb;

    /// <summary>
    /// Personaje que tiene la bomba.
    /// </summary>
    private ICharacterBomb characterBomb;

    [SerializeField] ushort[] rewardScore = new ushort[] { 30, 10, 5, 0 };
    [SerializeField] ushort maxPlayer = 4;

    /// <summary>
    /// Numero maximo de jugadores
    /// </summary>
    public UInt16 MaxPlayer { get { return maxPlayer; } }

    private bool isGameOver;
    public bool GameHasStart { get; private set; }

    [SerializeField] Transform[] spawnPoints = new Transform[4];

    void Awake()
    {
        Instance = this;
        CharacterLobbyManager.OnLoaded += CharacterLobbyManager_OnLoaded;
        CharacterLobbyManager.OnSaved += CharacterLobbyManager_OnSaved;

        CharacterLobbyManager.LoadPlayersData();
    }

    /// <summary>
    /// De momento el controlador no hace nada cuando se guarda exitosamente el archivo json.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void CharacterLobbyManager_OnSaved(object sender, bool e)
    {
        //throw new NotImplementedException();
    }

    /// <summary>
    /// Crear los jugadores a partir de los datos del json
    /// Si esta en debug: entonces llama la funcion por defecto con 4 jugadores.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="sucessful"></param>
    private void CharacterLobbyManager_OnLoaded(object sender, bool sucessful)
    {
        if (!sucessful)
        {
            #if (INTERNAL_DEBUG)
            {
                StartGame(4);
                return;
            }
            #else
            {
                Debug.LogError("NO hay datos de jugadores que cargar");
                EndGame();
                return;
            }
            #endif
        }

        PlayerInfo[] lobbyPlayers = CharacterLobbyManager.PlayersInfo;
        ICharacterBomb[] tempCharacters = new ICharacterBomb[lobbyPlayers.Length];
        for (int i = 0; i < tempCharacters.Length; i++)
        {
            tempCharacters[i] = Instantiate(characterPrefab, Vector3.zero, Quaternion.identity).GetComponent<ICharacterBomb>();
            tempCharacters[i].GetTransform().gameObject.SetActive(false);

            // Nombre, tag y color provisionales. La idea es agregarlos respecto al orden en que se unieron a la partida
            tempCharacters[i].ChangeColour(playerColours[i]);
            tempCharacters[i].GetTransform().gameObject.name = "PlayerBomb0" + (i + 1);
            tempCharacters[i].GetTransform().gameObject.tag = "P" + (i + 1);
            //

            tempCharacters[i].ID = lobbyPlayers[i].id;
            tempCharacters[i].GetTransform().GetComponent<CustomCharacterInput>().SetInputType(lobbyPlayers[i].Input);
        }

        #if(INTERNAL_DEBUG)
        // Inicia el juego con los jugadores creados.
        StartGame(tempCharacters);
        #endif
    }

    /// <summary>
    /// Inicializa el juego con una cantidad de jugadores determinda
    /// </summary>
    /// <param name="numberOfPlayers"></param>
    public void StartGame(int numberOfPlayers)
    {
        numberOfPlayers = Mathf.Clamp(numberOfPlayers, 1, MaxPlayer);
        ICharacterBomb[] tempCharacters = new ICharacterBomb[numberOfPlayers];
        for (int i = 0; i < tempCharacters.Length; i++)
        {
            tempCharacters[i] = Instantiate(characterPrefab, Vector3.zero, Quaternion.identity).GetComponent<ICharacterBomb>();
            tempCharacters[i].GetTransform().gameObject.SetActive(false); 
            tempCharacters[i].ChangeColour(playerColours[i]);
            tempCharacters[i].GetTransform().gameObject.name = "PlayerBomb0" + (i + 1);
            tempCharacters[i].GetTransform().gameObject.tag = "P"+(i+1);
            tempCharacters[i].GetTransform().GetComponent<CustomCharacterInput>().SetInputType((InputType)i);
        }

        StartGame(tempCharacters);
    }

    /// <summary>
    /// Initialize Minigame and Respawn Players
    /// </summary>
    /// <param name="characters"></param>
    public void StartGame(ICharacter[] characters)
    {
        try
        {
            theBomb = FindObjectOfType<Bomb>();
        }
        catch (NullReferenceException )
        {
            theBomb = Instantiate(bombPrefab, Vector3.zero, Quaternion.identity);
        }

        isGameOver = false;
        initialCharacters = characters;
        List<Transform> activeSpwanPoint = new List<Transform>();
        activePlayers = new List<ICharacterBomb>();
        activePlayers.AddRange(characters as ICharacterBomb[]);

        activeSpwanPoint.AddRange(spawnPoints);
        for(ushort i = 0; i < ActivePlayers.Length; i++)
        {
            int index = new Random().Next(activeSpwanPoint.Count);
            SpawnPlayer(i, activeSpwanPoint[index]);
            activeSpwanPoint.RemoveAt(index);
        }

        SetInitialBomb();

        GameHasStart = true;
        OnStartGame?.Invoke(this, new StartGameEventArgs(GAME_MODE));

    }

    public void SetInitialBomb()
    {
        //if (characterBomb != null && characterBomb.HasBomb())
        //{
        //    characterBomb.RemoveBomb();
        //}
        characterBomb = activePlayers[new Random().Next(activePlayers.Count)];
        characterBomb.AddBomb(ref theBomb);
    }

    /// <summary>
    /// Clean Game Data and Start the game.
    /// </summary>
    public void RestartGame()
    {
        /// TO-DO
        ///CLEAN GAME DATA.
        StartGame(initialCharacters);
    }

    /// <summary>
    /// End the game. Raise Winner and Score.
    /// </summary>
    public void EndGame()
    {
        print("Game has end");
        GameHasStart = false;
        isGameOver = true;
        /// TO-DO Order characters by the moment they died
        OnEndGame?.Invoke(this, new EndGameEventArgs(null));

    }

    /// <summary>
    /// Un/Pause the game according to value parameter
    /// </summary>
    /// <param name="value"></param>
    public void PauseGame(bool value)
    {
        OnPauseGame?.Invoke(this, new PauseGameEventArgs(value));
    }

    /// <summary>
    /// Spawn an Active Character by its index on Active player List
    /// </summary>
    /// <param name="index"></param>
    public void SpawnPlayer(int index)
    {
        SpawnPlayer(index, spawnPoints[new Random().Next(spawnPoints.Length)]);
    }

    /// <summary>
    /// Spawn an Active Character at specific point
    /// </summary>
    /// <param name="index"></param>
    /// <param name="point"></param>
    public void SpawnPlayer(int index, Transform point)
    {
        activePlayers[index].GetTransform().position = point.position;
        activePlayers[index].GetTransform().gameObject.SetActive(true);
    }

    /// <summary>
    /// Revisa si los jugadores estan vivos.
    /// Si sobrevive uno, entonces se gana el juego 
    /// </summary>
    private void CheckForAlivePlayers()
    {
        if (!GameHasStart) { return; }

        if(!isGameOver && activePlayers.Count == 1)
        {
            EndGame();
            return;
        }

        for(int i = 0; i < activePlayers.Count; i++)
        {
            if(activePlayers[i] != null && activePlayers[i].Life <= 0)
            {
                GameObject tempPlayer = activePlayers[i].GetTransform().gameObject;
                if(activePlayers[i] == characterBomb)
                {
                    Debug.Log(activePlayers[i].GetTransform().name + " HAS THE BOMB");
                    activePlayers.RemoveAt(i);
                    SetInitialBomb();
                }
                Destroy(tempPlayer, 0.5F);
            }
        }
    }

    void Update()
    {
        if (isGameOver) { return; }

        CheckForAlivePlayers();
    }

}

