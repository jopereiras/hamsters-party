﻿using UnityEngine;

public class RankingElement
{
    public readonly UnityEngine.Sprite sprite;
    public readonly string name;
    public ushort score;

    public RankingElement(Sprite sprite, string name, ushort score)
    {
        this.sprite = sprite;
        this.name = name;
        this.score = score;
    }
}
