﻿using UnityEngine;

public class CharacterBomb : Character, ICharacterBomb
{

    /// <summary>
    /// La bomba que tiene el personaje
    /// </summary>
    private Bomb bomb;

    /// <summary>
    /// Agrega la bomba a este personaje
    /// </summary>
    public void AddBomb(ref Bomb bomb)
    {
        bomb.TargetCharacter = this;
        this.bomb = bomb;
    }


    /// <summary>
    /// Pregunta si este personaje tiene  la bomba
    /// </summary>
    /// <returns></returns>
    public bool HasBomb()
    {
        return bomb != null;
    }

    /// <summary>
    /// Elimina la bomba de este personaje
    /// </summary>
    public void RemoveBomb()
    {
        bomb.TargetCharacter = null;
        bomb.Reset();
        bomb = null;
    }

    protected override void Start()
    {
        base.Start();
        BombGameController.Instance.OnEndGame += Instance_OnEndGame;
    }

    private void Instance_OnEndGame(object sender, EndGameEventArgs e)
    {
        canJump = false;
        canMove = false;
        CanShoot = false;
    }

    /// <summary>
    /// Agrega una fuerza que repele a ambos personajes al traspasarse la bomba
    /// </summary>
    /// <param name="direction"></param>
    public void AddDamageForce(int direction)
    {
        print("DIRECTION " + direction);
        rig.velocity = Vector2.zero;
        Vector2 force = new Vector2(100 , 6);
        rig.AddForce(force, ForceMode2D.Impulse);
        //StartCoroutine(WaitToResetInput(1.5f));
        SetDamage(0);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Bomb"))
        {
            ICharacterBomb otherCharacter = collision.transform.parent.GetComponent<ICharacterBomb>();
            if(otherCharacter != null && otherCharacter.HasBomb())
            {
                print("CHANGE BOMB");
                otherCharacter.RemoveBomb();

                ///Signo del eje x entre la distancia de este objeto y la bomba.
                int direction = (int)-Mathf.Sign(collision.transform.position.x - transform.position.x);
                AddDamageForce(direction);
                otherCharacter.AddDamageForce(-direction);

                AddBomb(ref BombGameController.Instance.theBomb);
            }
            else if(otherCharacter ==null)
            {
                print("OTHER CHARACTER IS NULL");
            }
        }
    }

}
