﻿using UnityEngine;

public class Balas : MonoBehaviour {

    private Rigidbody2D balard2d;
    public float balaspeed;
    public float balalife;


    private void Awake()
    {
        balard2d = GetComponent<Rigidbody2D>();
    }
    private void Start()
    {
        balard2d.velocity = new Vector2(balaspeed, balard2d.velocity.y);
    }
    private void Update()
    {
        Destroy(gameObject, balalife);
    }



}
