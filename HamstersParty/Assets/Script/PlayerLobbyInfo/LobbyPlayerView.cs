﻿using UnityEngine;

namespace HamsterParty.Lobby
{
    [RequireComponent(typeof(Animator))]
    public class LobbyPlayerView : MonoBehaviour
    {
        const string ANI_TAG = "login";
        const string ANI_IDLE_STATE = "Idle";
        const string ANI_SHOW_STATE = "Show";

        public bool IsFree = true;
        public float AniSpeed
        {
            get { return anim.speed; }
            set
            {
                if (value < 0) { return; }
                anim.speed = value;
            }
        }
        Animator anim;

        private void Awake()
        {
            anim = GetComponent<Animator>();
        }
        

        public void Show()
        {
            anim.SetBool(ANI_TAG, true);
            IsFree = false;
        }
        public void Hide()
        {
            anim.SetBool(ANI_TAG, false);
        }
    }
}
