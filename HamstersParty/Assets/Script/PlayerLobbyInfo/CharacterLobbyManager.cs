﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;
using System;
using System.IO;
using System.Collections.Generic;

#if(INTERNAL_DEBUG)
using Wenzil.Console;
#endif

namespace HamsterParty.Lobby
{
    /// <summary>
    /// Ingresa y Elimina personajes del juego
    /// Posee la lista de personajes dentro del juego
    /// Modifica el archivo Json con esa información
    /// </summary>
    public static class CharacterLobbyManager 
    {
        public const int MAX_PLAYERS = 4;
        private const string FILE_NAME = "temp_player_data.json";

        //public static event EventHandler<PlayerInOutEventArgs> OnPlayerInOut;
        public static event EventHandler<bool> OnSaved;
        public static event EventHandler<bool> OnLoaded;

        static List<PlayerInfo> players = new List<PlayerInfo>();
        static readonly InputType[] inputArray = new InputType[]
        {
            InputType.Keyboard,
            InputType.Joystick1,
            InputType.Joystick2,
            InputType.Joystick3,
            InputType.Joystick4
        };
        static readonly List<InputType> freeInputList;
        static readonly string dataPath;

        /*De momento todos los jugadores tiene el control
        ///// <summary>
        ///// EL jugador que tiene el control del menu.
        ///// </summary>
        //public static PlayerInfo MasterPlayer { get; private set; }
        */

        public static PlayerInfo[] PlayersInfo
        {
            get { return players.ToArray(); }
        }

        static CharacterLobbyManager()
        {
            //Agrega los comandos a la consola del juego.
            #if (INTERNAL_DEBUG)

            ConsoleCommandsDatabase.RegisterCommand(CommandAddPlayer.NAME, new ConsoleCommandCallback(CommandAddPlayer.Execute), CommandAddPlayer.DESCRIPTION, CommandAddPlayer.USAGE);
            ConsoleCommandsDatabase.RegisterCommand(CommandRemovePlayer.NAME, new ConsoleCommandCallback(CommandRemovePlayer.Execute), CommandRemovePlayer.DESCRIPTION, CommandRemovePlayer.USAGE);
            ConsoleCommandsDatabase.RegisterCommand(CommandShowInfo.NAME, new ConsoleCommandCallback(CommandShowInfo.Execute), CommandShowInfo.DESCRIPTION, CommandShowInfo.USAGE);
            ConsoleCommandsDatabase.RegisterCommand(CommandSavePlayersData.NAME, new ConsoleCommandCallback(CommandSavePlayersData.Execute), CommandSavePlayersData.DESCRIPTION, CommandSavePlayersData.USAGE);
            ConsoleCommandsDatabase.RegisterCommand(CommandLoadPlayersData.NAME, new ConsoleCommandCallback(CommandLoadPlayersData.Execute), CommandLoadPlayersData.DESCRIPTION, CommandLoadPlayersData.USAGE);

            #endif

            freeInputList = new List<InputType>(inputArray);
            // Aquí es donde se guarda el json
            dataPath = $"{UnityEngine.Application.temporaryCachePath}/{FILE_NAME}";

            #if (UNITY_EDITOR)

            UnityEngine.Debug.Log("Player data Path " + dataPath);

            #endif
        }

        public static bool AddPlayer(PlayerInfo data)
        {
            if(players.Count == MAX_PLAYERS || data == null) { return false; }

            if(LookForCopy(data.id) != null)
            {
                UnityEngine.Debug.LogError("Estas tratando de agregar una copia de un jugador");
                return false;
            }

            players.Add(data);
            return true;
        }

        private static PlayerInfo LookForCopy(int id)
        {
            return players.Find((PlayerInfo p) => p.id == id);
        }
        public static bool RemovePlayer(int id)
        {
            PlayerInfo p = LookForCopy(id);
            if(p == null) { return false; }
            else
            {
                players.Remove(p);
                return true;
            }
        }

        public static bool LoadPlayersData()
        {
            if (!File.Exists(dataPath)) { return false; }

            try
            {
                using (StreamReader file = new StreamReader(dataPath))
                {
                    string json = JToken.Parse(file.ReadToEnd()).ToString();
                    JArray array = JArray.Parse(json);

                    List<PlayerInfo> p = new List<PlayerInfo>();

                    List<float> colour = new List<float>();
                    int id = -1;
                    int score = 0;
                    InputType input = InputType.Keyboard;

                    foreach (JObject content in array.Children<JObject>())
                    {
                        foreach (JProperty prop in content.Properties())
                        {
                            switch (prop.Name)
                            {
                                case "id":
                                    id = (int)prop.Value;
                                    break;

                                case "Input":
                                    input = (InputType)((int)prop.Value);
                                    break;
                                case "Score":
                                    score = (int)prop.Value;
                                    break;
                                case "colourfloat":
                                    colour = new List<float>();
                                    foreach (JValue value in prop.Values())
                                    {
                                        colour.Add((float)value);
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                        p.Add(new PlayerInfo(id, input, colour.ToArray()));
                    }

                    p.TrimExcess();
                    players = p;
                }
                OnLoaded?.Invoke(null, true);
                return true;
            }
            catch(IOException)
            {
                OnLoaded?.Invoke(null, false);
                return false;
            }
        }
        public static bool SavePlayersData()
        {
            string jSonCharacters = JsonConvert.SerializeObject(players, Formatting.Indented);

            try
            {
                using (StreamWriter file = File.CreateText(dataPath))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(file, jSonCharacters);
                    OnSaved?.Invoke(null, true);
                    return true;
                }
            }
            catch (IOException)
            {
                OnSaved?.Invoke(null, false);
                return false;
            }
        }

        /// <summary>
        /// Elimina el archivo temporal con los datos del jugador
        /// </summary>
        public static void Clear()
        {
            if (File.Exists(dataPath))
            {
                File.Delete(dataPath);
            }
        }
    }

    //public class PlayerInOutEventArgs : EventArgs
    //{
    //    public readonly bool isEntry;
    //    public readonly PlayerInfo character;

    //    public PlayerInOutEventArgs(bool isEntry, PlayerInfo character)
    //    {
    //        this.isEntry = isEntry;
    //        this.character = character;
    //    }
    //}
}
