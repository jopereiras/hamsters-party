﻿using System;
using UnityEngine;

namespace HamsterParty.Lobby
{
    public class LobbyPlayerJoin
    {
        public const float HOLD_TIME = 1f;

        public event EventHandler<bool> OnAddedRemoved;

        /// <summary>
        /// bool representa si se agrega (true) o se quita (false) al jugador.
        /// </summary>
        public event EventHandler OnAddUp, OnAddDown, OnRemoveUp, OnRemoveDown;

        float timeCheck = 0f;
        readonly string confirmButton, cancelButton;
        public readonly InputType input;

        public bool IsComplete { get; private set; }
        public int Position { get; set; } = -1;

        public void CheckInput()
        {
            CheckToAdd();
            CheckToRemove();
        }

        private void CheckToAdd()
        {
            if (IsComplete) { return; }

            if(Input.GetButtonDown(confirmButton) )
            {
                OnAddDown?.Invoke(this, EventArgs.Empty);
            }

            if (Input.GetButton(confirmButton))
            {
                timeCheck += Time.deltaTime;
                if(timeCheck >= HOLD_TIME)
                {
                    IsComplete = true;
                    timeCheck = HOLD_TIME;
                    OnAddedRemoved?.Invoke(this, true);
                }
                return;
            }
            if(Input.GetButtonUp(confirmButton))
            {
                timeCheck = 0f;
                OnAddUp?.Invoke(this, EventArgs.Empty);
            }

        }
        private void CheckToRemove()
        {
            if (!IsComplete) { return; }

            if (Input.GetButtonDown(cancelButton))
            {
                OnRemoveDown?.Invoke(this, EventArgs.Empty);
            }

            if (Input.GetButton(cancelButton))
            {
                timeCheck -= Time.deltaTime;
                if(timeCheck <= 0)
                {
                    timeCheck = 0f;
                    IsComplete = false;
                    OnAddedRemoved?.Invoke(this, false);
                }
            }

            if(Input.GetButtonUp(cancelButton))
            {
                timeCheck = HOLD_TIME;
                IsComplete = true;
                OnRemoveUp?.Invoke(this, EventArgs.Empty);
            }
        }

        public LobbyPlayerJoin(string confirmButton, string cancelButton, InputType input)
        {
            this.confirmButton = confirmButton;
            this.cancelButton = cancelButton;
            this.input = input;
        }
    }
}
