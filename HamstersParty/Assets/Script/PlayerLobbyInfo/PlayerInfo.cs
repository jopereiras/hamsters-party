﻿using System;
using Newtonsoft.Json;
using UnityEngine;

namespace HamsterParty.Lobby
{

    /// <summary>
    /// Datos del jugador. Se debe escribir al empezar y terminar un minijuego o partida.
    /// Son los que van al .json
    /// </summary>
    public class PlayerInfo
    {
        public readonly int id;

        [JsonIgnore]
        public Color colour;

        public float[] colourfloat
        {
            get
            {
                return new float[]
                {
                    colour.r, colour.g, colour.b, 1f
                };
            }
        }


        private int score = 0;
        /// <summary>
        /// Propiedad de prueba, luego se agregaran el puntaje segun minijuego.
        /// </summary>
        public int Score
        {
            get { return score; }
            set
            {
                if(value < 0 || value == score || value > int.MaxValue) { return; }
                score = value;
            }
        }
        public InputType Input { get; set; } = InputType.Keyboard;

        [JsonConstructor]
        public PlayerInfo(int id, InputType inputType, float[] color)
        {
            this.id = id;
            Input = inputType;
            colour = new Color(color[0], color[1], color[2], 1f);
        }

        public PlayerInfo(InputType inputType, Color colour)
        {
            id = Guid.NewGuid().GetHashCode();
            Input = inputType;
            this.colour = colour;
        }

    }

}
