﻿using System;
using UnityEngine;
using Wenzil.Console.Commands;

namespace HamsterParty.Lobby
{

    public static class CommandAddPlayer
    {
        public const string NAME = "PLAYER.INFO.ADD";
        public const string DESCRIPTION = "Agrega una instancia PlayerInfo al sistema. Esta no es guardada";
        public const string USAGE = "PLAYER.INFO.ADD [input (string) int[3]rgb colour]";

        public static string Execute(params string[] args)
        {
            if (args.Length == 0 || args.Length < 2)
            {
                return HelpCommand.Execute(CommandAddPlayer.NAME);
            }

            int[] colour = new int[3];
            InputType input;
            if (!Enum.TryParse(args[0], out input) || 
                !Int32.TryParse(args[1], out colour[0]) ||
                !Int32.TryParse(args[2], out colour[1]) ||
                !Int32.TryParse(args[3], out colour[2]))
            {
                return "Uno o más de los valores asignados no son enteros o es negativo";
            }

            PlayerInfo player = new PlayerInfo(input, new Color(colour[0], colour[1], colour[2]));
            if (!CharacterLobbyManager.AddPlayer(player))
            {
                return @"No se pudo agregar los datos del jugador. 
                     Probablemente haya superado el máximo permitido";
            }

            return $"Datos del jugador agregados exitosamente. Player id = <b>{player.id}</b>";
        }
    }

}
