﻿using System;
using Wenzil.Console.Commands;

namespace HamsterParty.Lobby
{

    public static class CommandRemovePlayer
    {
        public const string NAME = "PLAYER.INFO.REMOVE";
        public const string DESCRIPTION = "Elimina una instancia PlayerInfo de la lista de jugadores. Esta lista no es guardada";
        public const string USAGE = "PLAYER.INFO.REMOVE [player id]";

        public static string Execute(params string[] args)
        {
            if(args.Length == 0)
            {
                return HelpCommand.Execute(CommandRemovePlayer.NAME);
            }

            int id;
            if(!Int32.TryParse(args[0], out id) || id <0)
            {
                return "Error: El valor asignado no es un entero o es negativo";
            }

            if (!CharacterLobbyManager.RemovePlayer(id))
            {
                return $"Errror: No se encuentra un jugador con id {id}";
            }

            return $"Datos del jugador <b>{id}</b> eliminados correctamente";
        }
    }

}

