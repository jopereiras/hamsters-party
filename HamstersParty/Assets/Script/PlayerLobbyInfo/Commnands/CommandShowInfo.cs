﻿using System.Text;

namespace HamsterParty.Lobby
{

    public static class CommandShowInfo
    {
        public const string NAME = "PLAYER.INFO.SHOW";
        public const string DESCRIPTION = "Muestra el id e input de los datos temporales de los jugadores";
        public const string USAGE = "PLAYER.INFO.SHOW";

        private const string FORMAT =
            @"  
    <b>ID</b>: {0}.
    <b>INPUT</b>: {1}";
        private static readonly StringBuilder result = new StringBuilder();

        public static string Execute(params string[] args)
        {
            result.Clear();

            PlayerInfo[] players = CharacterLobbyManager.PlayersInfo;

            if (players.Length == 0)
            {
                return "No hay jugadores en ejecucion.";
            }
            foreach (PlayerInfo player in players)
            {
                result.Append(string.Format(FORMAT, player.id, player.Input.ToString()));
            }

            return result.ToString();
        }
    }

}
