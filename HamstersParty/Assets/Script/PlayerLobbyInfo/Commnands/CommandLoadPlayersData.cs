﻿namespace HamsterParty.Lobby
{

    public static class CommandLoadPlayersData
    {
        public const string NAME = "PLAYERS.INFO.LOAD";
        public const string DESCRIPTION = "Carga un archivo .json con los datos temporales de los jugadores.";
        public const string USAGE = "PLAYER.INFO.LOAD";

        public static string Execute(params string[] args)
        {
            return (CharacterLobbyManager.LoadPlayersData()) ?
                "<b>Cargado correctamente</b>" :
                "<b>Error al cargar</b>";
        }
    }

}
