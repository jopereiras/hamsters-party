﻿namespace HamsterParty.Lobby
{

    public static class CommandSavePlayersData
    {
        public const string NAME = "PLAYERS.INFO.SAVE";
        public const string DESCRIPTION = "Guarda un archivo .json con los datos temporales de los jugadores.";
        public const string USAGE = "PLAYER.INFO.SAVE";

        public static string Execute(params string[] args)
        {
            return (CharacterLobbyManager.SavePlayersData()) ?
                "<b>Guardado correctamente</b>" :
                "<b>Error al guardar</b>";
        }
    }

}
