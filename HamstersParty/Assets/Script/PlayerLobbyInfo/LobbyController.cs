﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace HamsterParty.Lobby
{
    /// <summary>
    /// Agrega jugadores, mediante input, al juego y carga la escena inicial
    /// </summary>
    public class LobbyController : MonoBehaviour
    {
        [SerializeField] string nextMinigameScene = "DeathMatchScene";

        [SerializeField] LobbyView view;

        [SerializeField] string confirmButton = "Confirm";
        [SerializeField] string cancelButton = "Cancel";
        private bool canAddPlayers = true;

        private LobbyPlayerJoin[] playersToJoin = new LobbyPlayerJoin[5];
        public Dictionary<int,LobbyPlayerJoin> JoinedPlayers { get; private set; } = new Dictionary<int, LobbyPlayerJoin>(4);

        private void Awake()
        {
            CharacterLobbyManager.Clear();
            for (int i = 0; i < playersToJoin.Length; i++)
            {
                if (i == 0)
                {
                    playersToJoin[0] = new LobbyPlayerJoin($"K_{confirmButton}", $"K_{cancelButton}", (InputType)i);
                }
                else
                {
                    playersToJoin[i] = new LobbyPlayerJoin($"J_0{i}_{confirmButton}", $"J_0{i}_{cancelButton}", (InputType)i);
                }
                playersToJoin[i].OnAddedRemoved += LobbyController_OnAddedRemoved;
                playersToJoin[i].OnAddDown += LobbyController_OnAddDown;
                playersToJoin[i].OnAddUp += LobbyController_OnAddUp;
                playersToJoin[i].OnRemoveDown += LobbyController_OnRemoveDown;
                playersToJoin[i].OnRemoveUp += LobbyController_OnRemoveUp;
            }
        }

        private void LobbyController_OnRemoveUp(object sender, EventArgs e)
        {
            KeyValuePair<int, LobbyPlayerJoin> foundPlayer = JoinedPlayers.FirstOrDefault(p => p.Value == (LobbyPlayerJoin)sender);
            if (foundPlayer.Value != null)
            {
                view.ShowItem(foundPlayer.Key);
            }
        }

        private void LobbyController_OnRemoveDown(object sender, EventArgs e)
        {
            KeyValuePair<int, LobbyPlayerJoin> foundPlayer = JoinedPlayers.FirstOrDefault(p => p.Value == (LobbyPlayerJoin)sender);
            if (foundPlayer.Value != null)
            {
                view.HideItem(foundPlayer.Key);
            }
        }

        private void LobbyController_OnAddUp(object sender, EventArgs e)
        {
            KeyValuePair<int, LobbyPlayerJoin> foundPlayer = JoinedPlayers.FirstOrDefault(p => p.Value == (LobbyPlayerJoin)sender);
            if (foundPlayer.Value != null)
            {
                view.HideItem(foundPlayer.Key);
            }
        }
        private void LobbyController_OnAddDown(object sender, EventArgs e)
        {
            LobbyPlayerJoin player = (LobbyPlayerJoin)sender;
            KeyValuePair<int, LobbyPlayerJoin> foundPlayer = JoinedPlayers.FirstOrDefault(p => p.Value == player);
            if(foundPlayer.Value != null)
            {
                view.ShowItem(foundPlayer.Key);
                return;
            }

            int index = view.GetFreeSlot();
            if(index > -1)
            {
                JoinedPlayers.Add(index, player);
                view.ShowItem(index);
            }
        }

        private void RemoveLobbyPlayer(LobbyPlayerJoin player)
        {
            KeyValuePair<int, LobbyPlayerJoin> foundPlayer = JoinedPlayers.FirstOrDefault(p => p.Value == player);
            if(foundPlayer.Value != null)
            {
                view.DeleteItem(foundPlayer.Key);
                JoinedPlayers.Remove(foundPlayer.Key);
            }
        }

        private void LobbyController_OnAddedRemoved(object sender, bool addPlayer)
        {
            if(!addPlayer)
            {
                KeyValuePair<int, LobbyPlayerJoin> foundPlayer = JoinedPlayers.FirstOrDefault(p => p.Value == (LobbyPlayerJoin)sender);
                if (foundPlayer.Value != null)
                {
                    JoinedPlayers.Remove(foundPlayer.Key);
                    view.DeleteItem(foundPlayer.Key);
                }
            }
        }

        private void Update()
        {
            if (!canAddPlayers) { return; }

            playersToJoin[0].CheckInput();
            playersToJoin[1].CheckInput();
            playersToJoin[2].CheckInput();
            playersToJoin[3].CheckInput();
            playersToJoin[4].CheckInput();
        }

        public void GoToGame()
        {
            if(JoinedPlayers.Count < 2)
            {
                #if(INTERNAL_DEBUG)
                Debug.Log("NO hay suficientes jugadores");
                #endif
                return;
            }

            canAddPlayers = false;
            /// Remove Listeners
            for (int i = 0; i < playersToJoin.Length; i++)
            {
                playersToJoin[i].OnAddedRemoved -= LobbyController_OnAddedRemoved;
                playersToJoin[i].OnAddDown -= LobbyController_OnAddDown;
                playersToJoin[i].OnAddUp -= LobbyController_OnAddUp;
                playersToJoin[i].OnRemoveDown -= LobbyController_OnRemoveDown;
                playersToJoin[i].OnRemoveUp -= LobbyController_OnRemoveUp;
            }

            for (int i = 0; i < JoinedPlayers.Count; i++)
            {
                if(JoinedPlayers[i] == null) { continue; }
                CharacterLobbyManager.AddPlayer(new PlayerInfo(JoinedPlayers[i].input, view.playersColours[i]));
            }


            CharacterLobbyManager.OnSaved += CharacterLobbyManager_OnSaved;
            CharacterLobbyManager.SavePlayersData();
        }

        private void CharacterLobbyManager_OnSaved(object sender, bool success)
        {
            if (success)
            {
                if (nextMinigameScene != "")
                {
                    CharacterLobbyManager.OnSaved -= CharacterLobbyManager_OnSaved;
                    SceneManager.LoadSceneAsync(nextMinigameScene);
                }
            }
        }

    }

  
}
