﻿using UnityEngine;
using UnityEngine.UI;

namespace HamsterParty.Lobby
{
    public class LobbyView : MonoBehaviour
    {
        [SerializeField] LobbyPlayerView[] playersView;
        public Color[] playersColours = new Color[4];

        private void Awake()
        {
            for (int i = 0; i < playersView.Length; i++)
            {
                Image image = playersView[i].GetComponent<Image>();
                if(image != null)
                {
                    image.color = playersColours[i];
                }
            }
        }

        /// <summary>
        /// Añade un jugador al inventario, ejecutando la animación de añadir
        /// Entrega la posición del jugador a añadir
        /// </summary>
        public int GetFreeSlot()
        {
            int index = -1;
            for (int i = 0; i < playersView.Length; i++)
            {
                if(playersView[i].IsFree)
                {
                    index = i;
                    break;
                }
            }
            return index;
        }

        public void ShowItem(int slot)
        {
            if (slot < 0 || slot >= playersView.Length) { return; }

            playersView[slot].Show();
            playersView[slot].IsFree = false;
        }
        public void HideItem(int slot)
        {
            if (slot < 0 || slot >= playersView.Length) { return; }

            playersView[slot].Hide();
            //playersView[slot].waitToHide = false;
        }

        public bool DeleteItem(int slot)
        {
            /// No existe la vista, porque el indice esta fuera del alcance
            if(slot < 0 || slot >= playersView.Length) { return false; }

            playersView[slot].Hide();
            playersView[slot].IsFree = true;
            //playersView[slot].waitToHide = false;
            return true;
        }
    }
}
