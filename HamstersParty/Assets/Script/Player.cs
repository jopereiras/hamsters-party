﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Obsolete("Usar clase Character, esta clase se borrara despues de factorizar.")]
public class Player : MonoBehaviour
{

    public float Velocidad;
    public float VelocidadMax;
    private Rigidbody2D Rb2d;
    Botones Bot;
    


    void Start()
    {
        Bot = GetComponent<Botones>();
        Rb2d = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        Bot.PlayerShooting();
        Bot.Salto();
          
    }
    public void FixedUpdate()
    {
        Vector2 Friccion = Rb2d.velocity;
        Friccion.x *= 0.9f;
        
            Rb2d.velocity = Friccion;
        

        float h = Input.GetAxis("Horizontal");

        Rb2d.AddForce(Vector2.right * Velocidad * h);

        float limiteVelocidad = Mathf.Clamp(Rb2d.velocity.x, -VelocidadMax, VelocidadMax);
        Rb2d.velocity = new Vector2(limiteVelocidad, Rb2d.velocity.y);

    }

}
