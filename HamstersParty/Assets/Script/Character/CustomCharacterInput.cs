﻿using UnityEngine;
using System.Text;

public class CustomCharacterInput : MonoBehaviour
{
    [SerializeField] Character character;
    [SerializeField] InputType inputType = InputType.Keyboard;

    const string HORIZONTAL_NAME = "_Horizontal";
    const string VERTICAL_NAME = "_Vertical";
    const string JUMP_NAME = "_Jump";
    const string SHOOT_NAME = "_Shoot";
    const string CROUCH_NAME = "_Crouch";

    private string horizontalAxis;
    private string verticalAxis;
    private string shootAxis;
    private string jumpAxis;
    private string crouchAxis;

    [SerializeField] float horizontalAxisSensitive = 0.1f;

    float hdir;

    private void Start()
    {
        SetInputType(inputType);
    }

    public void SetInputType(InputType inputType)
    {
        this.inputType = inputType;
        if(this.inputType == InputType.Keyboard)
        {
            horizontalAxis = "K" + HORIZONTAL_NAME;
            verticalAxis = "K" + VERTICAL_NAME;
            jumpAxis = "K" + JUMP_NAME;
            shootAxis = "K" + SHOOT_NAME;
            crouchAxis = "K" + CROUCH_NAME;
        }
        else
        {
            string valueString = ((int)this.inputType).ToString();
            horizontalAxis = "J_0" + valueString + HORIZONTAL_NAME;
            verticalAxis = "J_0" + valueString + VERTICAL_NAME;
            jumpAxis = "J_0" + valueString + JUMP_NAME;
            shootAxis = "J_0" + valueString + SHOOT_NAME;
            crouchAxis = "J_0" + valueString + CROUCH_NAME;
        }
    }

    private void FixedUpdate()
    {
        if (Input.GetButtonDown(jumpAxis))
        {
            character.Jump();
        }
        if (Input.GetButtonUp(jumpAxis))
        {
            character.FastGround();
        }

        hdir = Input.GetAxis(horizontalAxis);
        //if(gameObject.tag == "P2")
        //{
        //    print("JOYSTICK H DIR " + hdir);
        //}

        if(hdir < -horizontalAxisSensitive || hdir > horizontalAxisSensitive)
        {
            character.Move(hdir);
        }
        else if(horizontalAxisSensitive == 0.0f)
        {
            character.Move(hdir);
        }

        if (Input.GetAxis(shootAxis) > 0.8f)
        {
            character.Shoot();
        }

        if (Input.GetButton(crouchAxis) && !character.IsCrouch)
        {
            character.Crouch();
        }
        if (Input.GetButtonUp(crouchAxis))
        {
            character.StandUp();
        }

        if (Input.GetAxis(verticalAxis) < 0 && Input.GetButton(jumpAxis))
        {
           // Debug.Log("DOWWWWWN");
            character.DownJump();
        }
    }

}

public enum InputType
{
    Keyboard = 0,
    Joystick1 = 1,
    Joystick2 = 2,
    Joystick3 = 3,
    Joystick4 = 4,
}