﻿using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(SpriteRenderer))]
public class Character : MonoBehaviour, ICharacter
{
    public const int MAX_LIFE = 5;

    public event EventHandler<DamageEventHandler> OnDamage;

    protected Rigidbody2D rig;
    protected BoxCollider2D bodyCollider;
    protected SpriteRenderer rend;
    [SerializeField] FeetCollider feetCollider;
    [SerializeField] ShootComponenet shootProperty;
    [SerializeField] float damageCoolDown = 1f;

    private int id = -1;
    public int ID
    {
        get { return id; }
        set
        {
            if (id > 0 || value < 0) { return; }
            id = value;
        }
    }

    public int SpriteDirection
    {
        get { return (rend.flipX) ? -1 : 1; }
        set
        {
            if(value == 0) { return; }
            rend.flipX = (value > 0) ? false : true;
            if(shootProperty != null)
            {
                shootProperty.FlipShootPointX  = rend.flipX;
            }
        }
    }

    [SerializeField] int life = 3;
    public int Life
    {
        get { return life; }
        set
        {
            if(value < 0) { value = 0; }
            life = value;
        }
    }

    [Range(0.01f, 0.12f)] [SerializeField] float friction = 0.9f;

    [Header("Stand Up Properties")]
    [SerializeField] float speed    = 3f;
    [SerializeField] float maxSpeed = 7f;
    [SerializeField] float jumpFore = 5f;
    [SerializeField] float maxJump  = 7f;
    private float colliderSize;
    private float colliderOffset;

    [Header("Crouch Properties")]
    [SerializeField] float crouchSpeed = 3f;
    [SerializeField] float crouchMaxSpeed = 5f;
    [SerializeField] float crouchColliderHeight = 0.5F;
    [SerializeField] float crouchOffset = -0.25f;

    ///Cosas que solo se aplican a este prototipo
    [Header("Debug")]
    [SerializeField] Sprite normalSprite;
    [SerializeField] Sprite crouchSprite;

    protected bool isGrounded = false;
    public bool canJump     = true;
    public bool canMove     = true;
    public bool CanShoot
    {
        get { return (shootProperty == null) ? false : shootProperty.CanShoot; }
        set
        {
            if(shootProperty == null) { return; }
            shootProperty.CanShoot = value;
        }
    }
    public bool IsCrouch { get; private set; }

    protected virtual void Awake()
    {
        rend            = GetComponent<SpriteRenderer>();
        rig             = GetComponent<Rigidbody2D>();
        bodyCollider    = GetComponent<BoxCollider2D>();
        colliderSize    = bodyCollider.size.y;
        colliderOffset  = bodyCollider.offset.y;

        feetCollider.OnFeetCollision += FeetCollider_OnFeetCollision;
    }

    protected virtual void Start()
    {
        if (shootProperty != null)
        {
            shootProperty.bulletTag = gameObject.tag;
        }
    }

    private void FeetCollider_OnFeetCollision(object sender, FeetCollisionEventArgs e)
    {
        isGrounded = e.hasCollide;
    }

    public Transform GetTransform()
    {
        return transform;
    }

    public void Jump()
    {
        if (!canJump) { return; }
        if (!isGrounded) { return; }

        if (IsCrouch) { StandUp(); }

        rig.AddForce(Vector2.up * jumpFore, ForceMode2D.Impulse);
        rig.velocity = new Vector2(rig.velocity.x, Mathf.Clamp(rig.velocity.y, -maxJump, maxJump));
    }
    public void FastGround()
    {
        rig.AddForce(Vector2.down * (rig.velocity.y / 2), ForceMode2D.Impulse);
    }

    public void Crouch()
    {
        if (IsCrouch) { return; }
        rend.sprite = crouchSprite;

        bodyCollider.size = new Vector2(bodyCollider.size.x, crouchColliderHeight);
        bodyCollider.offset = new Vector2(bodyCollider.offset.x, crouchOffset);
        IsCrouch = true;
    }
    public void StandUp()
    {
        if (!IsCrouch) { return; }
        rend.sprite = normalSprite;

        bodyCollider.size = new Vector2(bodyCollider.size.x, colliderSize);
        bodyCollider.offset = new Vector2(bodyCollider.offset.x, colliderOffset);
        IsCrouch = false;
    }

    public void Move(float dir)
    {
        if (!canMove || dir == 0.0f) { return; }
        rig.AddForce(Vector2.right * dir * speed * 10);
        rig.velocity = new Vector2(Mathf.Clamp(rig.velocity.x, -maxSpeed, maxSpeed), rig.velocity.y);

        ChangeSpriteDirection((int)Mathf.Sign(dir));
    }

    public void ChangeSpriteDirection(int direction)
    {
        if(direction == 0) { return; }

        if (direction != SpriteDirection) { SpriteDirection = direction; }
    }

    void FixedUpdate()
    {
        Vector2 fir = rig.velocity;
        fir.x *= friction;
        rig.velocity = fir;
    }

    bool shoot = false;
    public void Shoot()
    {
        if (shoot == true) { return; }
        shoot = true;
        if (shootProperty == null) { return; }
        shootProperty.Shoot();
        StartCoroutine(StopShoot());
    }

    /// <summary>
    /// Cambia el color del icono que representa al player.
    /// Se supone que el icono es blanco.
    /// </summary>
    /// <param name="colour"></param>
    public void ChangeColour(Color colour)
    {
        rend.color = colour;
    }

    bool downjump = false;


    public void DownJump()
    {
        
        if (downjump) { return; }

        gameObject.layer = LayerMask.NameToLayer("Down");
        rig.AddForce(Vector2.down * (rig.velocity.y / 2), ForceMode2D.Impulse);
        downjump = true;
    }

    public void NodownJump()
    {
        if (!downjump) { return; }

        gameObject.layer = LayerMask.NameToLayer("Player");

        downjump = false;

    }

    bool isBlinking = false;
    public virtual void SetDamage(int damage)
    {
        life -= damage;
        OnDamage?.Invoke(this, new DamageEventHandler { DamageRecieved = damage, LifeLeft = life, TimeToBlink = 1 });

        ///BLINK HERE
        if(!isBlinking)
        {
            StartCoroutine(WaitToBlink(damageCoolDown));
        }

    }

    protected IEnumerator WaitToResetInput(float time)
    {
        canJump = false;
        canMove = false;
        CanShoot = false;

        yield return new WaitForSeconds(time);

        canJump = true;
        canMove = true;
        CanShoot = true;
    }

    protected IEnumerator WaitToBlink(float time)
    {
        canJump = false;
        canMove = false;
        CanShoot = false;

        bodyCollider.enabled = false;
        Color initialPlayerColour = rend.color;
        Color playerColour = rend.color;
        for (float i = 0.0f; i < time; i += 1f)
        {
            playerColour.a = 0.1f;
            rend.color = playerColour;
            yield return new WaitForSeconds(0.5f);
            rend.color = initialPlayerColour;
            yield return new WaitForSeconds(0.5f);
        }

        canJump = true;
        canMove = true;
        CanShoot = true;

        bodyCollider.enabled = true;
    }

    protected IEnumerator StopShoot()
    {
        yield return new WaitForSeconds(0.3f);
        shoot = false;
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        if(collider.gameObject.layer == LayerMask.NameToLayer("DeadZone"))
        {
            Life = -1;
            Debug.Log("DIE! DIE! !EID");
        }
    }

}
