﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Clase que permite que cualquier objeto dispare
/// </summary>
public class ShootComponenet : MonoBehaviour
{
    public const string P1_BULLET_TAG = "P1";
    public const string P2_BULLET_TAG = "P2";
    public const string P3_BULLET_TAG = "P3";
    public const string P4_BULLET_TAG = "P4";

    [SerializeField] Transform shootPoint;
    [SerializeField] Bullet bulletPrefab;
    SpriteRenderer rend;
    [HideInInspector]public string bulletTag = P1_BULLET_TAG;

    public Bullet Bullet
    {
        get { return bulletPrefab; }
        set
        {
            if(value == bulletPrefab || value == null) { return; }
            bulletPrefab = value;
        }
    }

    [Range(0.0f, 60f)] [SerializeField] float coolTime = 1f;
    public float CoolTime
    {
        get { return coolTime; }
        set
        {
            if(value < 0.0f) { value = 0.0f; }
            coolTime = value;
        }
    }

    bool canShoot = true;
    public bool CanShoot
    {
        get { return canShoot; }
        set
        {
            if(value == canShoot) { return; }
            if (value) { StopAllCoroutines(); }
            canShoot = value;
        }
    }

    private float shootInitialPositionX;
    private bool flipShootPointX;
    public bool FlipShootPointX
    {
        get { return flipShootPointX; }
        set
        {
            if(value == flipShootPointX) { return; }
            shootPoint.localPosition = (value) ?
                new Vector2(-shootInitialPositionX, shootPoint.localPosition.y):
                new Vector2(shootInitialPositionX, shootPoint.localPosition.y);

            int yRotation = (value) ? 180 : 0;
            shootPoint.localRotation = Quaternion.Euler(shootPoint.localRotation.eulerAngles.x, yRotation, shootPoint.localRotation.eulerAngles.z);

            if(rend != null) { rend.flipX = value; }
            flipShootPointX = value;
        }
    }

    private void Awake()
    {
        rend = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        shootInitialPositionX = shootPoint.localPosition.x;
    }
    
    public void Shoot()
    {
        if (!canShoot) { return; }

        Bullet bullet = Instantiate(bulletPrefab, shootPoint.position, shootPoint.rotation);
        bullet.tag = bulletTag;

        if(coolTime > 0.0f)
        {
            canShoot = false;
            StartCoroutine(WaitCoolTime(coolTime));
        }
    }

    IEnumerator WaitCoolTime(float time)
    {
        yield return new WaitForSeconds(time);
        canShoot = true;
    }
}
