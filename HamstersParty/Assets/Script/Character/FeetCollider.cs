﻿using System;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class FeetCollider : MonoBehaviour
{
    public event EventHandler<FeetCollisionEventArgs> OnFeetCollision;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(OnFeetCollision != null)
        {
            OnFeetCollision(this, new FeetCollisionEventArgs(true));
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (OnFeetCollision != null)
        {
            OnFeetCollision(this, new FeetCollisionEventArgs(false));
        }
    }
}

public class FeetCollisionEventArgs : EventArgs
{
    public readonly bool hasCollide;

    public FeetCollisionEventArgs(bool hasCollide)
    {
        this.hasCollide = hasCollide;
    }
}
