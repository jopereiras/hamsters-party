﻿using System;
using UnityEngine;

public interface IGameController
{
    event EventHandler<EndGameEventArgs> OnEndGame;
    event EventHandler<StartGameEventArgs> OnStartGame;
    event EventHandler<PauseGameEventArgs> OnPauseGame;

    UInt16 MaxPlayer { get; }
    ICharacter[] ActivePlayers { get; }

    [Obsolete("Esta funcion será privada luego de refactorizar. " +
    "Los jugadores deberian crearse a partir del json")]
    void StartGame(ICharacter[] characters);

    [Obsolete("Esta funcion será privada luego de refactorizar. " +
        "Los jugadores deberian crearse a partir del json")]
    void StartGame(int numberOfPlayers);

    // Luego de factorizar. esta interfaz deberia estar asi:
    // void StartGame()

    void RestartGame();
    void EndGame();
    void PauseGame(bool value);
    void SpawnPlayer(int index);
    void SpawnPlayer(int index, Transform point);
}

public class EndGameEventArgs : EventArgs
{
    public readonly RankingElement[] ranking;
    public readonly bool forceExit;

    public EndGameEventArgs(RankingElement[] ranking)
    {
        this.ranking = ranking;
    }

    public EndGameEventArgs(bool forceExit)
    {
        this.forceExit = forceExit;
    }
}
public class StartGameEventArgs : EventArgs
{
    public readonly string gameMode;

    public StartGameEventArgs(string gameMode)
    {
        this.gameMode = gameMode;
    }
}
public class PauseGameEventArgs : EventArgs
{
    public readonly bool value;

    public PauseGameEventArgs(bool value)
    {
        this.value = value;
    }
}

