﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Obsolete("Usar clase FeetCollider. Esta clase se borrara despues de factorizar")]
public class Colicion : MonoBehaviour {

    
    Botones Bot;
	
	void Start ()
    {
        
        Bot = GetComponentInParent<Botones>();
	}
	
	
	void Update () {
		
	}

    public void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Platform")
        {
            
            Bot.salto = false;
        }
    }

    public void OnCollisionExit2D(Collision2D col)
    {
        if (col.gameObject.tag == "Platform")
        {
            
            Bot.salto = true;
        }
    }
}
