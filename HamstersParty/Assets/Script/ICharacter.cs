﻿using System;

public interface ICharacter
{
    event EventHandler<DamageEventHandler> OnDamage;
    
    int ID { get; set; }

    int Life { get; set; }

    void Move(float dir);
    void Jump();
    void FastGround();
    void Shoot();
    void ChangeColour(UnityEngine.Color colour);
    void SetDamage(int damage);

    UnityEngine.Transform GetTransform();
}

/// <summary>
/// Argumentos cuando el personaje recive daño
/// </summary>
public class DamageEventHandler : EventArgs
{
    public float TimeToBlink { get; set; }
    public int DamageRecieved { get; set; }
    public int LifeLeft { get; set; }
}