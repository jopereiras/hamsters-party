﻿using UnityEngine;

public class PlayersLifeUI : MonoBehaviour
{
    const float FULL = 150f;
    int lineDivision = 5;
    [SerializeField] LineRenderer[] playersHealth;
    
    int activePlayers;
    bool hasInitialized = false;

    public void SetColor(Color color, int index)
    {
        playersHealth[index].material.color = color;
    }

    public void Initialize(int activePlayers, int lineDivision)
    {
        if(activePlayers < 1 || lineDivision < 1) { return; }

        this.activePlayers = activePlayers;
        this.lineDivision = lineDivision;

        hasInitialized = true;

        for (int i = 0; i < playersHealth.Length; i++)
        {
            ChangeByPlayersIndex(i, this.lineDivision);
            if(i > this.activePlayers)
            {
                playersHealth[i].gameObject.SetActive(false);
            }
        }

    }

    public void ChangeByPlayersIndex(int index, int value)
    {
        if (!hasInitialized)
        {
            Debug.LogWarning("Inicializa PlayersLifeUI con el método Initialize(int, int) primero");
            return;
        }

        if(index < 0 || index > activePlayers || value < 0) { return; }

        Vector3 pos = playersHealth[index].GetPosition(1);
        pos.x = (value * FULL) / lineDivision;
        playersHealth[index].SetPosition(1, pos);
    }
}
