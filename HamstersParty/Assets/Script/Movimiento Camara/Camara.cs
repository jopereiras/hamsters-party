﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camara : MonoBehaviour {

    public float speed = 4f;
    public Vector2 minCamPos, maxCamPos;
    bool Ejecutar = false;
    float speedRetur;
    Vector3 mov;
    void Start()
    {
        mov = new Vector3(0, 1, 0);
    }


    void Update()
    {
        
        transform.position = Vector3.MoveTowards(
            transform.position,
            transform.position + mov,
            speed * Time.deltaTime
            );



        transform.position = new Vector3(
            Mathf.Clamp(transform.position.x, minCamPos.x, maxCamPos.x),
            Mathf.Clamp(transform.position.y, minCamPos.y, maxCamPos.y),
            transform.position.z);
    }



    public void speedCamera()
    {
        if (Ejecutar) { return; }

        speedRetur = speed;
        speed = speed * 2;
        StartCoroutine(Stop());
        Ejecutar = true;
    }


    IEnumerator Stop()
    {
        yield return new WaitForSeconds(1.2f);

        speed = speedRetur;
        Ejecutar = false;
    }


}

